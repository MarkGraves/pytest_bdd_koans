from pytest_bdd import then
import pytest

# pytest-bdd whens are functions per pytest_bdd/steps.py @ line 139

@pytest.fixture()
def sample_then_fixture():
    return 'then'

def test_then_is_a_pytest_fixture(sample_then_fixture):
    assert(sample_then_fixture == 'then')

def test_then_does_not_accept_fixture_argument(sample_then_fixture):
    assert(sample_then_fixture == 'then')

    with pytest.raises(TypeError):
        then('I have a then statement', fixture='sample_then_fixture')
