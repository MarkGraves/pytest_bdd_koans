# First, we need an authorized user list to store user data in
#
# We create this as a pytest fixture called authorized_users_list
#

import pytest

@pytest.fixture()
def user_registration_input():
    user_input = {}
    user_input['email'] = 'email@domain.com'
    user_input['password'] = 'password'
    return user_input

@pytest.fixture(scope='session')
def expected_users_list(request):
    test_user = {}
    test_user['email'] = 'email@domain.com'
    test_user['password'] = 'password'
    return [test_user]


@pytest.fixture(scope='session')
def authorized_users_list(request):
    return []

# It starts by returning an empty list
def test_starting_with_an_empty_auth_list(authorized_users_list):
    assert(authorized_users_list == [])

# Then the user inputs a value, and the registration list is modified
def test_user_inputs_values_for_registration(user_registration_input, authorized_users_list, expected_users_list):
    authorized_users_list.append(user_registration_input)
    user = authorized_users_list[0]
    user_expected_email_input = 'email@domain.com'
    user_expected_password_input = 'password'
    # Test to make sure user put in a value for email
    assert('email' in user.keys())
    # Test to make sure user put in a value for password
    assert('password' in user.keys())
    # Make sure value for email address is expected value
    assert('email@domain.com' == user_expected_email_input)
    # Make sure value for password is expected values
    assert('password' == user_expected_password_input)
    # Make sure list of authorized users is updated
    assert(authorized_users_list == expected_users_list)

# Now we make sure the user is retained in our "database"
def test_retained_user_input_values(authorized_users_list,expected_users_list):
    assert(authorized_users_list == expected_users_list)



