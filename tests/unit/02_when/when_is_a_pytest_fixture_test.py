from pytest_bdd import when
import pytest

# pytest-bdd givens are fixtures per pytest_bdd/steps.py @ line 139
#
# pytest-bdd whens are fixtures, but they dont accept a fixture name

@pytest.fixture()
def sample_when_fixture():
    return 'when'

def test_when_is_a_pytest_fixture(sample_when_fixture):
    assert(sample_when_fixture == 'when')

def test_when_does_not_accept_fixture_argument(sample_when_fixture):
    assert(sample_when_fixture == 'when')

    with pytest.raises(TypeError):
        when('I have a when statement', fixture='sample_when_fixture')
