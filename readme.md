# pytest-bdd Koans

pytest-bdd is a plugin for pytest

As such it allows you to re-use pytest concepts (and code) from unit tests in the context of BDD

Work in this project builds off the pytest-koans project

# Goal:
The goal of this project is to mimic a basic registration form and landing page for an application.

It will take in an email address and a password and store them.

This will then be used in a web2py project to demonstrate usage within the web2py context